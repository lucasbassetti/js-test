describe('Calculadora', function() {

    var calculator = Calculator.getInstance(),
        calcArea = document.getElementById('calcArea');

    it('should be able to sum 2 values', function() {
        calculator.clear(calcArea);
        calculator.add("10", calcArea);
        calculator.setOperator("+");
        calculator.add("5", calcArea);
        calculator.calc(calcArea);

        expect(calcArea.innerHTML).toBe("15");
    });

    it('should be able to subtract 2 values', function() {

        calculator.clear(calcArea);
        calculator.add("10", calcArea);
        calculator.setOperator("-");
        calculator.add("5", calcArea);
        calculator.calc(calcArea);

        expect(calcArea.innerHTML).toBe("5");
    });

    it('should be able to multiple 2 values', function() {

        calculator.clear(calcArea);
        calculator.add("10", calcArea);
        calculator.setOperator("*");
        calculator.add("5", calcArea);
        calculator.calc(calcArea);

        expect(calcArea.innerHTML).toBe("50");
    });

    it('should be able to divide 2 values', function() {

        calculator.clear(calcArea);
        calculator.add("10", calcArea);
        calculator.setOperator("/");
        calculator.add("5", calcArea);
        calculator.calc(calcArea);

        expect(calcArea.innerHTML).toBe("2");
    });

    it('should be able to multiple and sum values', function() {

        calculator.clear(calcArea);
        calculator.add("10", calcArea);
        calculator.setOperator("*");
        calculator.add("5", calcArea);
        calculator.calc(calcArea);
        calculator.setOperator("+");
        calculator.add("2", calcArea);
        calculator.calc(calcArea);

        expect(calcArea.innerHTML).toBe("52");
    });

    it('should be able to set a operator', function() {

        calculator.setOperator("+");
        expect(calculator.operator).toBe("+");
    });

    it('should be able to clear the calculator', function() {

        calculator.add("10", calcArea);
        calculator.clear(calcArea);
        expect(calcArea.innerHTML).toBe("0");
    });

});
