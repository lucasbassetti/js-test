var Calculator = (function() {
    var instance;

    // initialize calculator
    function init() {

        return {
            operator: undefined,    // + - * /
            operatorSetted: false,  // check operator was setted
            calcExecuted: false,    // check calc executed
            totalValue: "0",        // total value
            currentValue: "0",      // current value

            // add number and comman in calculator output
            add: function(element, calcArea) {
                if(calcArea.innerHTML === '0' || this.operatorSetted || this.calcExecuted) {
                    calcArea.innerHTML = element;

                    // initalize total value
                    if(this.totalValue == "0") {
                        this.totalValue = this.currentValue;
                    }

                    this.operatorSetted = false;
                    this.calcExecuted = false;
                }
                else {
                    calcArea.innerHTML += element;
                }

                this.currentValue = calcArea.innerHTML;
            },

            // clear calculator output and reset vars
            clear: function(calcArea) {
                this.operator = undefined;
                this.operatorSetted = false;
                this.calcExecuted = false;
                this.currentValue = "0";
                this.totalValue = "0";
                calcArea.innerHTML = 0;
            },

            // performs the calculation
            calc: function(calcArea) {
                if(this.operator) {
                    var total = this.totalValue.replace(/,/g, '.') +
                                this.operator +
                                this.currentValue.replace(/,/g, '.');

                    total = eval(total);
                    this.totalValue = total.toString().replace(/\./g, ',');

                    calcArea.innerHTML = this.totalValue;
                    this.calcExecuted = true;
                }
            },

            // set new operator
            setOperator: function(operator) {
                this.operator = operator;
                this.operatorSetted = true;
            }


        }
    }

    return {
        getInstance: function () {
            if (!instance) {
                instance = init();
            }
            return instance;
        }
    };
})();
