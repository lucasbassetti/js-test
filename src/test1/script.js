(function() {
    var calculator = Calculator.getInstance();

   // add number event when click in number or comma button
    for(var i = 0, len = document.getElementsByClassName('element').length; i < len; i++) {
        document.getElementsByClassName('element')[i].addEventListener("click", function(){
            calculator.add(this.innerHTML, document.getElementById('calcArea'));
        });
    }

    // add set operator event when click in an operator
    for(var i = 0, len = document.getElementsByClassName('operator').length; i < len; i++) {
        document.getElementsByClassName('operator')[i].addEventListener("click", function(){
            calculator.setOperator(this.innerHTML);
        });
    }

    // add clear event when click in clear button
    document.getElementById("clearOperator").addEventListener("click", function(){
        calculator.clear(document.getElementById('calcArea'));
    });

    // add calculates event when click in '=' button
    document.getElementById("calcOperator").addEventListener("click", function(){
        calculator.calc(document.getElementById('calcArea'));
    });
})();
