describe("Sections", function () {
    // beforeEach(module('app'));

    var reactiveResult = jasmine.createSpyObj('reactiveResult', ['attach']);
    var $reactive = jasmine.createSpy('$reactive').and.returnValue(reactiveResult);
    var controller = {};
    beforeEach(function () {
        module(function ($provide) {
            $provide.factory('$reactive', $reactive);
        });
        module('app');
    });

    it('Should call attach', function () {
        $reactive(controller).attach();
        expect($reactive).toHaveBeenCalledWith(controller);
        expect(reactiveResult.attach).toHaveBeenCalled();
    }) ;

});
