angular.module('app', ['angular-meteor']).directive('content', content);

function content() {
    var directive = {
        restrict: 'EA',
        templateUrl: 'client/content.html',
        controller: contentController,
        controllerAs: 'content'
    }

    return directive;
};

contentController.$inject = ['$scope', '$reactive'];

function contentController($scope, $reactive) {
    $reactive(this).attach($scope);

    Session.set('sections', false);

    $scope.sections = [];

    // reactive functions
    $scope.helpers({
        mySections: () => {
           Session.get('sections');
           setTimeout(function() {
               $('select').material_select();
           }, 100);
           return $scope.sections;
       },
       json: () => {
           Session.get('sections');
           return JSON.stringify($scope.sections, null, 2);
       }
    })

    // enable sections drag
    $('.draggable').draggable({
        revert: function(event, ui) {
            $(this).data("uiDraggable").originalPosition = {
                top : 0,
                left : 0
            };
            return true;
        }
    });

    // enable drop a section
    $('#droppable').droppable({
        accept: '.draggable',
        activeClass: "ui-state-hover",
        drop: function(event, ui){

            // get section id
            var id = $(ui.draggable).attr('id');

            // create section1
            if(id === 'section1') {
                $scope.sections.push({
                    type: 1,
                    edit: true,
                    data: {
                        title: '',
                        subtitle: '',
                        image: ''
                    }
                });
            }

            // create section2
            if(id === 'section2') {
                $scope.sections.push({
                    type: 2,
                    edit: true,
                    data: {
                        title: '',
                        subtitle: '',
                        background: '',
                        button: {
                            label: '',
                            link: '',
                            target: '_self'
                        }
                    }
                });
            }

            // create section3
            if(id === 'section3') {
                $scope.sections.push({
                    type: 3,
                    edit: true,
                    data: {
                        createdAt: '',
                        image1: '',
                        image2: '',
                        image3: ''
                    }
                });
            }

            // perform reactive when create
            Session.set('sections', !Session.set('sections'));
        }
    });

    // perform reactive when save
    $scope.save = function() {
        Session.set('sections', !Session.set('sections'));
    }

    // perform reactive when edit
    $scope.edit = function() {
        Session.set('sections', !Session.set('sections'));
    }

    // perform reactive when delete
    $scope.delete = function(index) {
        $scope.sections.splice(index, 1);
        Session.set('sections', !Session.set('sections'));
    }

}
