## Referências Técnicas

1. Quais foram os últimos dois livros técnicos que você leu?

Learning JavaScript Design Patterns e Segredos do Ninja javascript

2. Quais foram os últimos dois framework javascript que você trabalhou?

MeteorJS e AngularJS

3. Descreva os principais pontos positivos de seu framework favorito.

> Construído em cima do Node.js;
> Javascript em ambas camadas: cliente e servidor.
> Open-source

4. Descreva os principais pontos negativos do seu framework favorito.

> Acredito que seja o refresh. Quando a aplicação crescre, o refresh demora um pouco.

5. O que é código de qualidade para você?

> Códgio Legível, Indentado, Escalável e Reaproveitável.

## Conhecimento de desenvolvimento

1. O que é GIT?

É um sistema de controle de versão distribuído e gerenciamento de código fonte.

2. Descreva um workflow simples de trabalho utilizando GIT.

> "git init" para iniciar o projeto
> "git add" para adionar os arquivos no repositório local e permitir o commit .
> "git commit" para descrever as mudanças e preparar para enviar ao repositório remoto.
> "git push" envia as alterações e reza para não dar conflito :)

3. Como você avalia seu grau de conhecimento em Orientação a objeto?

Razoável, trabalhei alguns anos com java.

4. O que é Dependency Injection?

É a capacidade de importar automaticente uma depêndencia que será utilizada em outro escopo do código.

5. O significa a sigla S.O.L.I.D?

Um conjuto de principios da Orientação a Objetos.

6. O que é BDD e qual sua importância para o processo de desenvolvimento?

Idealizado a partir do TDD, BDD (Behavior Driven Development) é um processo de testes ligados a integração e comportamento da aplicação, permitindo garantir a integridade e qualidade do software.

7. Fale sobre Design Patterns.

Design Pattern é uma solução geral (como um template) reutilizável para um problema que ocorre com frequência dentro de um determinado contexto no projeto de software.

8. Discorra sobre práticas que você considera boas no desenvolvimento javascript.

> Projete antes de codificar. Pensar melhor no problema pode render um bom tempo e gerar um código melhor
> Não reinvente a roda. Use o que já existe, provavelmente vai ser melhor do que você criar um novo.
> Mantenha-se sempre atualizado. Não fique preso em algo do passado, é bom sempre se atualizar e melhorar :)
> Não é porque o código funcionou que ele está bom. Tente sempre melhorar o que foi feito.
> Pergunte sempre que tiver dúvida. A vergonha pode ser pior por ter feito algo enganado.
